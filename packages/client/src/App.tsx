import React from 'react';
import { Provider } from 'react-redux';
import styled from 'styled-components';

import { EditorComponent } from './components/editor';
import { Examples } from './components/examples';
import { HeaderComponent } from './components/header';
import { OutputComponent } from './components/output';
import configureStore from './configure-store';

const store = configureStore();

const Container = styled.div`
  display: flex;
`;

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <HeaderComponent></HeaderComponent>
      <Container>
        <Examples></Examples>
        <EditorComponent></EditorComponent>
        <OutputComponent></OutputComponent>
      </Container>
    </Provider>
  );
};

export default App;
