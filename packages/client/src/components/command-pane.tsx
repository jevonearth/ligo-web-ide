import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { AppState, ChangeEntrypointAction, ChangeParametersAction, ChangeStorageAction, Command } from '../redux/reducers';

const Container = styled.div`
  padding: 25px;
`;

const Group = styled.div`
  display: flex;
  flex-direction: column;
`;

const Label = styled.label`
  font-size: 1rem;
  color: rgba(153, 153, 153, 1);
`;

const Input = styled.input`
  margin: 10px 0 20px 0;
  background-color: #eff7ff;
  border-style: none;
  border-bottom: 5px solid #e1f1ff;
  padding: 10px;
  font-size: 1rem;
  font-family: Menlo, Monaco, 'Courier New', monospace;

  &:focus {
    background-color: #e1f1ff;
  }
`;

const Textarea = styled.textarea`
  resize: vertical;
  margin: 10px 0 20px 0;
  background-color: #eff7ff;
  border-style: none;
  border-bottom: 5px solid #e1f1ff;
  padding: 10px;
  font-size: 1rem;
  font-family: Menlo, Monaco, 'Courier New', monospace;

  &:focus {
    background-color: #e1f1ff;
  }
`;

export const CommandPaneComponent = () => {
  const dispatch = useDispatch();
  const command = useSelector<AppState, AppState['command']>(
    state => state.command
  );
  const entrypoint = useSelector<AppState, AppState['entrypoint']>(
    state => state.entrypoint
  );
  const parameters = useSelector<AppState, AppState['parameters']>(
    state => state.parameters
  );
  const storage = useSelector<AppState, AppState['storage']>(
    state => state.storage
  );

  return (
    <Container>
      <Group>
        <Label htmlFor="entrypoint">Entrypoint</Label>
        <Input
          id="entrypoint"
          value={entrypoint}
          onChange={ev =>
            dispatch({ ...new ChangeEntrypointAction(ev.target.value) })
          }
        ></Input>
      </Group>
      {command === Command.Simulate && (
        <Group>
          <Label htmlFor="parameters">Parameters</Label>
          <Textarea
            id="parameters"
            rows={9}
            value={parameters}
            onChange={ev =>
              dispatch({ ...new ChangeParametersAction(ev.target.value) })
            }
          ></Textarea>
        </Group>
      )}
      {[Command.Simulate, Command.Originate].includes(command) && (
        <Group>
          <Label htmlFor="storage">Storage</Label>
          <Textarea
            id="storage"
            rows={9}
            value={storage}
            onChange={ev =>
              dispatch({ ...new ChangeStorageAction(ev.target.value) })
            }
          ></Textarea>
        </Group>
      )}
    </Container>
  );
};
