import { faCaretDown, faCaretUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import OutsideClickHandler from 'react-outside-click-handler';
import { useDispatch, useSelector } from 'react-redux';
import styled, { css } from 'styled-components';

import { AppState, ChangeCommandAction, Command } from '../redux/reducers';

interface Style {
  overlay: boolean;
}

const Container = styled.div`
  display: flex;
  flex: 1;
  height: 40px;
  position: relative;
`;

const Header = styled.div`
  display: flex;
  flex: 1;
  cursor: pointer;
  user-select: none;
`;

const Title = styled.div`
  display: flex;
  align-items: center;
  flex: 1;
  min-width: 150px;
  height: 40px;
  border-bottom: 5px solid #e1f1ff;
  padding-left: 10px;

  &:hover {
    background-color: #fedace;
    border-bottom: 5px solid #fc683a;
  }

  ${(props: Style) =>
    props.overlay &&
    css`
      background-color: #fedace;
      border-bottom: 5px solid #fc683a;
      border-radius: 3px 0 0 0;
    `}
`;

const Arrow = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 40px;
  min-width: 40px;
  border-bottom: 5px solid #e1f1ff;

  &:hover {
    background-color: #fedace;
    border-bottom: 5px solid #fc683a;
  }

  ${(props: Style) =>
    props.overlay &&
    css`
      background-color: #fedace;
      border-bottom: 5px solid #fc683a;
    `}
`;

const List = styled.ul`
  position: absolute;
  list-style-type: none;
  background: white;
  width: 100%;
  margin: 0;
  padding: 0;
  box-shadow: 1px 3px 10px 0px rgba(153, 153, 153, 0.4);
  border-radius: 3px;
  z-index: 2;
`;

const Option = styled.li`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 10px;
  height: 40px;
  cursor: pointer;
  user-select: none;

  &:first-child {
    border-radius: 3px 3px 0 0;
  }

  &:last-child {
    border-radius: 0 0 3px 3px;
  }

  &:hover {
    background-color: #fedace;
    font-weight: 600;
  }
`;

export const CommandSelectComponent = (params: {
  overlay: boolean;
  onDefaultAction: () => void;
  onOptionSelected: () => void;
}) => {
  const OPTIONS = {
    [Command.Compile]: 'Compile',
    [Command.Simulate]: 'Dry-Run',
    [Command.Originate]: 'Deploy'
  };

  const dispatch = useDispatch();
  const command = useSelector<AppState, AppState['command']>(
    state => state.command
  );
  const [opened, open] = useState(false);

  const moveOptionToTop = (option: Command) => {
    return Object.keys(OPTIONS).reduce(
      (list, entry) => {
        if (entry === option) {
          list.unshift(entry);
        } else {
          list.push(entry as Command);
        }
        return list;
      },
      [] as Command[]
    );
  };

  const selectOption = (option: Command) => {
    if (command !== option) {
      dispatch({ ...new ChangeCommandAction(option) });
    }
    open(false);
    params.onOptionSelected();
  };

  return (
    <Container>
      {!opened && (
        <Header>
          <Title
            overlay={params.overlay}
            onClick={() => params.onDefaultAction()}
          >
            <span>{OPTIONS[command]}</span>
          </Title>
          <Arrow overlay={params.overlay} onClick={() => open(true)}>
            <FontAwesomeIcon icon={faCaretDown} size="lg" color="#fc683a" />
          </Arrow>
        </Header>
      )}
      {opened && (
        <OutsideClickHandler onOutsideClick={() => open(false)}>
          <List>
            {moveOptionToTop(command).map(option => (
              <Option key={`${option}`} onClick={() => selectOption(option)}>
                <span>{OPTIONS[option]}</span>
                {command === option && (
                  <FontAwesomeIcon icon={faCaretUp} size="lg" color="#fc683a" />
                )}
              </Option>
            ))}
          </List>
        </OutsideClickHandler>
      )}
    </Container>
  );
};
