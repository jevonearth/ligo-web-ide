import React from 'react';
import styled from 'styled-components';

import { MonacoComponent } from './monaco';
import { ShareComponent } from './share';
import { SyntaxSelectComponent } from './syntax-select';

const Container = styled.div`
  flex: 2;
`;

const Header = styled.div`
  display: flex;
`;

export const EditorComponent = () => {
  return (
    <Container>
      <Header>
        <SyntaxSelectComponent></SyntaxSelectComponent>
        <ShareComponent></ShareComponent>
      </Header>
      <MonacoComponent></MonacoComponent>
    </Container>
  );
};
