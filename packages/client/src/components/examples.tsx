import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { AppState, SelectExampleAction } from '../redux/reducers';

const bgColor = 'transparent';
const borderSize = 5;
const verticalPadding = 10;

const MenuItem = styled.div<{ selected: boolean }>`
  padding: ${verticalPadding}px 20px;
  height: 20px;
  display: flex;
  align-items: center;
  cursor: pointer;
  background-color: ${props =>
    props.selected ? 'var(--blue_trans1)' : bgColor};
  border-left: ${`${borderSize}px solid ${bgColor}`};
  border-left-color: ${props => (props.selected ? 'var(--blue)' : bgColor)};

  :first-child {
    margin-top: ${props => (props.selected ? '0' : `-${borderSize}px`)};
  }

  :hover {
    background-color: ${props =>
      props.selected ? 'var(--blue_trans1)' : 'var(--blue_trans2)'};
    border-left: ${`${borderSize}px solid ${bgColor}`};
    border-left-color: ${props =>
      props.selected ? 'var(--blue)' : 'transparent'};
    :first-child {
      margin-top: ${props => (props.selected ? '0' : `-${borderSize}px`)};
      padding-top: ${props =>
        props.selected ? `${verticalPadding}px` : `${borderSize}px`};
      border-top: ${props =>
        props.selected ? '' : `${borderSize}px solid var(--blue_trans1)`};
    }
  }
`;

const MenuContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const Header = styled.div<{ firstChildSelected: boolean }>`
  border-bottom: ${props =>
    props.firstChildSelected ? '' : '5px solid var(--blue_trans1)'};
  min-height: 40px;
  padding: 0 10px;
  display: flex;
  align-items: center;
`;

export const Examples = () => {
  const examples = useSelector<AppState, AppState['examples']>(
    (state: AppState) => state.examples
  );
  const selectedExample = useSelector<AppState, AppState['selectedExample']>(
    (state: AppState) => state.selectedExample
  );
  const dispatch = useDispatch();

  return (
    <div>
      <Header firstChildSelected={Object.keys(examples)[0] === selectedExample}>
        <span>Examples</span>
      </Header>
      <MenuContainer>
        {Object.keys(examples).map(exampleID => {
          const example = examples[exampleID];
          return (
            <MenuItem
              key={exampleID}
              selected={exampleID === selectedExample}
              onClick={() =>
                dispatch({ ...new SelectExampleAction(exampleID) })
              }
            >
              {example.name}
            </MenuItem>
          );
        })}
      </MenuContainer>
    </div>
  );
};
