import React from 'react';
import styled, { css } from 'styled-components';

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  padding: 8px 0px;
  font-family: 'DM Sans', 'Open Sans', sans-serif;
`;

const Group = styled.div`
  display: flex;
  align-items: center;
`;

const Logo = styled.div`
  font-size: 1.25em;
`;

const Link = styled.a`
  text-decoration: none;
  color: black;
  padding: 6px 10px;

  &:hover {
    color: #3aa0ff;
  }

  ${(props: { versionStyle?: boolean }) =>
    props.versionStyle &&
    css`
      background-color: #efefef;
      font-weight: 600;
      margin-left: 50px;

      &:hover {
        color: black;
      }
    `}
`;

export const HeaderComponent = () => {
  return (
    <Container>
      <Group>
        <Link href="https://ligolang.org">
          <Logo>LIGO</Logo>
        </Link>
        <Link versionStyle href="https://ligolang.org/versions">
          next
        </Link>
      </Group>
      <Group>
        <Link href="https://ligolang.org/docs/setup/installation">Docs</Link>
        <Link href="https://ligolang.org/docs/tutorials/get-started/tezos-taco-shop-smart-contract">
          Tutorials
        </Link>
        <Link href="https://ligolang.org/blog">Blog</Link>
        <Link href="https://ligolang.org/docs/contributors/origin">
          Contribute
        </Link>
      </Group>
    </Container>
  );
};
