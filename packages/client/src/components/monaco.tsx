import * as monaco from 'monaco-editor';
import React, { useEffect, useRef } from 'react';
import { useDispatch, useStore } from 'react-redux';
import styled from 'styled-components';

import { AppState, ChangeCodeAction } from '../redux/reducers';

const Container = styled.div`
  height: 80vh;
`;

export const MonacoComponent = () => {
  let containerRef = useRef(null);
  const store = useStore();
  const dispatch = useDispatch();

  useEffect(() => {
    const cleanupFunc: Array<() => void> = [];
    const { code, language } = store.getState();
    const model = monaco.editor.createModel(code, language);

    monaco.editor.defineTheme('ligoTheme', {
      base: 'vs',
      inherit: true,
      rules: [],
      colors: {
        'editor.lineHighlightBackground': '#fedace',
        'editorLineNumber.foreground': '#888'
      }
    });

    monaco.editor.setTheme('ligoTheme');

    const editor = monaco.editor.create(
      (containerRef.current as unknown) as HTMLElement,
      {
        model: model,
        automaticLayout: true,
        minimap: {
          enabled: false
        }
      }
    );

    const { dispose } = editor.onDidChangeModelContent(() => {
      dispatch({ ...new ChangeCodeAction(editor.getValue()) });
    });

    cleanupFunc.push(dispose);

    cleanupFunc.push(
      store.subscribe(() => {
        const { code, language }: AppState = store.getState();
        if (code !== editor.getValue()) {
          editor.setValue(code);
        }

        if (language !== model.getModeId()) {
          monaco.editor.setModelLanguage(model, language);
        }
      })
    );

    return function cleanUp() {
      cleanupFunc.forEach(f => f());
    };
  }, [store, dispatch]);

  return <Container id="editor" ref={containerRef}></Container>;
};
