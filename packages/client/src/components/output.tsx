import React, { useState } from 'react';
import OutsideClickHandler from 'react-outside-click-handler';
import { useDispatch, useSelector } from 'react-redux';
import { PushSpinner } from 'react-spinners-kit';
import { Dispatch } from 'redux';
import styled, { css } from 'styled-components';

import { AppState, ChangeContractAction, ChangeOutputAction, Command, MichelsonFormat } from '../redux/reducers';
import { compile, originate, simulate } from '../services/api';
import { CommandPaneComponent } from './command-pane';
import { CommandSelectComponent } from './command-select';

interface Style {
  overlayStyle?: boolean;
  loadingStyle?: boolean;
}

interface LoadingState {
  loading: boolean;
  message: string;
}

const DONE_LOADING: LoadingState = { loading: false, message: '' };

const Container = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;

  ${(props: Style) =>
    props.overlayStyle &&
    css`
      box-shadow: 1px 3px 10px 0px rgba(153, 153, 153, 0.4);
      border-radius: 3px;
      z-index: 2;
      align-self: flex-start;
    `}
`;

const Header = styled.div`
  display: flex;
`;

const Button = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  height: 40px;
  min-width: 150px;
  color: #fc683a;
  border-bottom: 5px solid #fc683a;
  cursor: pointer;
  user-select: none;

  &:hover {
    color: white;
    background-color: #fc683a;
  }

  ${(props: Style) =>
    props.overlayStyle &&
    css`
      background-color: #fedace;
      border-radius: 0 3px 0 0;
    `}
`;

const CancelButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 40px;
  width: 100px;
  color: white;
  background-color: #fc683a;
  cursor: pointer;
  user-select: none;
  margin: 30px;
`;

const OutputContainer = styled.div`
  font-family: Menlo, Monaco, 'Courier New', monospace;
  overflow: scroll;
  height: 80vh;
  display: flex;
`;

const Output = styled.div`
  flex: 1;
  padding: 10px;
  display: flex;
`;

const LoadingContainer = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const LoadingMessage = styled.div`
  padding: 10px 0;
`;

function getErrorMessage(ex: any): string {
  if (ex instanceof Error) {
    return ex.message;
  } else if (ex.response && ex.response.data) {
    return ex.response.data.error;
  }

  return JSON.stringify(ex);
}

abstract class CancellableAction {
  private cancelled = false;

  onCancel = () => {};

  cancel() {
    this.cancelled = true;
  }

  isCancelled() {
    return this.cancelled;
  }

  abstract getAction(): any;
}

class CompileAction extends CancellableAction {
  constructor(private updateLoading: (loading: LoadingState) => void) {
    super();
  }

  getAction() {
    return async (dispatch: Dispatch, getState: () => AppState) => {
      this.updateLoading({ loading: true, message: 'Compiling contract...' });

      try {
        const { language, code, entrypoint } = getState();
        const result = await compile(language, code, entrypoint);

        if (this.isCancelled()) {
          this.onCancel();
          return;
        }

        dispatch({ ...new ChangeOutputAction(result.code) });
      } catch (ex) {
        if (this.isCancelled()) {
          this.onCancel();
          return;
        }
        dispatch({
          ...new ChangeOutputAction(`Error: ${getErrorMessage(ex)}`)
        });
      }

      this.updateLoading(DONE_LOADING);
    };
  }
}

class DryRunAction extends CancellableAction {
  constructor(private updateLoading: (loading: LoadingState) => void) {
    super();
  }

  getAction() {
    return async (dispatch: Dispatch, getState: () => AppState) => {
      this.updateLoading({
        loading: true,
        message: 'Waiting for dry run results...'
      });

      try {
        const { language, code, entrypoint, parameters, storage } = getState();
        const result = await simulate(
          language,
          code,
          entrypoint,
          parameters,
          storage
        );
        if (this.isCancelled()) {
          this.onCancel();
          return;
        }
        dispatch({ ...new ChangeOutputAction(result.output) });
      } catch (ex) {
        if (this.isCancelled()) {
          this.onCancel();
          return;
        }
        dispatch({
          ...new ChangeOutputAction(`Error: ${getErrorMessage(ex)}`)
        });
      }

      this.updateLoading(DONE_LOADING);
    };
  }
}

class DeployAction extends CancellableAction {
  constructor(private updateLoading: (loading: LoadingState) => void) {
    super();
  }

  getAction() {
    return async (dispatch: Dispatch, getState: () => AppState) => {
      const { language, code, entrypoint, storage } = getState();

      try {
        this.updateLoading({ loading: true, message: 'Compiling contract...' });
        const result = await compile(
          language,
          code,
          entrypoint,
          MichelsonFormat.Micheline
        );

        if (this.isCancelled()) {
          this.onCancel();
          return;
        }

        this.updateLoading({
          loading: true,
          message: 'Waiting for TezBridge signer...'
        });
        const op = await originate(result.code, storage);

        if (this.isCancelled()) {
          this.onCancel();
          return;
        }

        this.updateLoading({
          loading: true,
          message: 'Deploying to babylon network...'
        });
        const contract = await op.contract();

        if (this.isCancelled()) {
          this.onCancel();
          return;
        }

        dispatch({ ...new ChangeContractAction(contract.address) });
      } catch (ex) {
        if (this.isCancelled()) {
          this.onCancel();
          return;
        }
        dispatch({
          ...new ChangeOutputAction(`Error: ${getErrorMessage(ex)}`)
        });
      }

      this.updateLoading(DONE_LOADING);
    };
  }
}

export const OutputComponent = () => {
  const dispatch = useDispatch();
  const [showOverlay, setShowOverlay] = useState(false);
  const [restoreOverlay, setRestoreOverlay] = useState(false);
  const [loading, updateLoading] = useState({ loading: false, message: '' });
  const [dispatchedAction, setDispatchedAction] = useState();

  const output = useSelector<AppState, AppState['output']>(
    state => state.output
  );
  const contract = useSelector<AppState, AppState['contract']>(
    state => state.contract
  );

  const command = useSelector<AppState, AppState['command']>(
    state => state.command
  );

  return (
    <Container overlayStyle={showOverlay}>
      <OutsideClickHandler onOutsideClick={() => setShowOverlay(false)}>
        <Header>
          <CommandSelectComponent
            overlay={showOverlay}
            onDefaultAction={() => setShowOverlay(!showOverlay)}
            onOptionSelected={() => setShowOverlay(true)}
          ></CommandSelectComponent>
          <Button
            overlayStyle={showOverlay}
            onClick={() => {
              if (dispatchedAction) {
                dispatchedAction.cancel();
              }

              let newAction = null;

              if (command === Command.Compile) {
                newAction = new CompileAction(updateLoading);
              } else if (command === Command.Simulate) {
                newAction = new DryRunAction(updateLoading);
              } else if (command === Command.Originate) {
                newAction = new DeployAction(updateLoading);
              } else {
                throw new Error('Unsupported command');
              }

              if (newAction) {
                dispatch(newAction.getAction());
                setDispatchedAction(newAction);
                setShowOverlay(false);
              }
            }}
          >
            Run
          </Button>
        </Header>
        {showOverlay && <CommandPaneComponent></CommandPaneComponent>}
      </OutsideClickHandler>

      {!showOverlay && (
        <OutputContainer>
          <Output>
            {loading.loading && (
              <LoadingContainer>
                <PushSpinner size={50} color="#fedace" />
                <LoadingMessage>{loading.message}</LoadingMessage>
                <CancelButton
                  onClick={() => {
                    dispatchedAction.cancel();
                    updateLoading(DONE_LOADING);
                  }}
                >
                  Cancel
                </CancelButton>
              </LoadingContainer>
            )}
            {!loading.loading &&
              ((output.length !== 0 && output) ||
                (contract.length !== 0 && (
                  <span>
                    The contract was successfully deployed to the babylon
                    network.
                    <br />
                    <br />
                    View it on{' '}
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      href={`https://better-call.dev/babylon/${contract}`}
                    >
                      Better Call Dev
                    </a>
                    !
                  </span>
                )))}
          </Output>
        </OutputContainer>
      )}
    </Container>
  );
};
