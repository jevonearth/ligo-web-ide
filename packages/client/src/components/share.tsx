import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Dispatch } from 'redux';
import styled from 'styled-components';

import { AppState, ChangeShareLink } from '../redux/reducers';
import { share } from '../services/api';

const ShareContainer = styled.div`
  flex: 1;
  display: flex;
`;

const Button = styled.div<{ highlight: boolean }>`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  height: 40px;
  max-width: 150px;
  min-width: 150px;
  color: #fc683a;
  border-bottom: 5px solid #fc683a;
  cursor: pointer;
  user-select: none;

  &:hover {
    color: white;
    background-color: #fc683a;
    border-bottom: 5px solid #fc683a;
  }
`;

const Input = styled.input<{ highlight: boolean }>`
  flex: 2;
  border: 0px;
  padding: 10px;
  font-size: 1rem;

  &:focus {
    outline: none;
  }

  border-bottom: ${props =>
    props.highlight ? '5px solid #fc683a' : '5px solid #e1f1ff'};
`;

const shareAction = () => {
  return async function(dispatch: Dispatch, getState: () => AppState) {
    try {
      const { hash } = await share(getState());
      dispatch({ ...new ChangeShareLink(hash) });
    } catch (ex) {}
  };
};

function copy(element: HTMLInputElement) {
  element.select();
  element.setSelectionRange(0, 99999);
  document.execCommand('copy');
}

export const ShareComponent = () => {
  const inputEl = useRef<HTMLInputElement>(null);
  const dispatch = useDispatch();
  const shareLink = useSelector<AppState, AppState['shareLink']>(
    state => state.shareLink
  );
  useEffect(() => {
    if (inputEl.current) {
      copy(inputEl.current);
    }
  }, [shareLink]);
  return (
    <ShareContainer>
      <Button highlight={!!shareLink} onClick={() => dispatch(shareAction())}>
        Share
      </Button>
      <Input
        highlight={!!shareLink}
        readOnly
        ref={inputEl}
        value={shareLink ? `${window.location.origin}/p/${shareLink}` : ''}
      ></Input>
    </ShareContainer>
  );
};
