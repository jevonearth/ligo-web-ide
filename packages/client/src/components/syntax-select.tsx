import { faCaretDown, faCaretUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import OutsideClickHandler from 'react-outside-click-handler';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { AppState, ChangeLanguageAction, Language } from '../redux/reducers';

const Container = styled.div`
  display: flex;
  position: relative;
  min-width: 200px;
`;

const Header = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 10px;

  cursor: pointer;
  user-select: none;
  border-bottom: 5px solid #e1f1ff;

  &:hover {
    background-color: #fedace;
    border-bottom: 5px solid #fc683a;
  }
`;

const List = styled.ul`
  position: absolute;
  list-style-type: none;
  background: white;
  width: 100%;
  margin: 0;
  padding: 0;
  box-shadow: 1px 3px 10px 0px rgba(153, 153, 153, 0.4);
  border-radius: 3px;
  z-index: 2;
`;

const Option = styled.li`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 10px;
  height: 40px;
  cursor: pointer;
  user-select: none;

  &:first-child {
    border-radius: 3px 3px 0 0;
  }

  &:last-child {
    border-radius: 0 0 3px 3px;
  }

  &:hover {
    background-color: #fedace;
    font-weight: 600;
  }
`;

export const SyntaxSelectComponent = () => {
  const OPTIONS = {
    [Language.PascaLigo]: 'PascaLIGO',
    [Language.CameLigo]: 'CameLIGO'
  };

  const moveOptionToTop = (option: Language) => {
    return Object.keys(OPTIONS).reduce(
      (list, entry) => {
        if (entry === option) {
          list.unshift(entry);
        } else {
          list.push(entry as Language);
        }
        return list;
      },
      [] as Language[]
    );
  };

  const language = useSelector<AppState, AppState['language']>(
    state => state.language
  );
  const dispatch = useDispatch();
  const [opened, open] = useState(false);

  const selectOption = (option: Language) => {
    if (language !== option) {
      dispatch({ ...new ChangeLanguageAction(option) });
    }
    open(false);
  };

  return (
    <Container>
      {!opened && (
        <Header onClick={() => open(true)}>
          <span>{OPTIONS[language]}</span>
          <FontAwesomeIcon icon={faCaretDown} size="lg" color="#fc683a" />
        </Header>
      )}
      {opened && (
        <OutsideClickHandler onOutsideClick={() => open(false)}>
          <List>
            {moveOptionToTop(language).map(option => (
              <Option key={option} onClick={() => selectOption(option)}>
                <span>{OPTIONS[option]}</span>
                {language === option && (
                  <FontAwesomeIcon icon={faCaretUp} size="lg" color="#fc683a" />
                )}
              </Option>
            ))}
          </List>
        </OutsideClickHandler>
      )}
    </Container>
  );
};
