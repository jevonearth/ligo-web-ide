import { applyMiddleware, createStore, Middleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import rootReducer from './redux/reducers';

export default function configureStore() {
  const store = createStore(
    rootReducer,
    applyMiddleware(ReduxThunk, cleanRouteOnAction)
  );

  return store;
}

const cleanRouteOnAction: Middleware = store => next => action => {
  const { shareLink } = store.getState();
  next(action);
  const state = store.getState();
  if (
    shareLink !== undefined &&
    state.shareLink === undefined &&
    window.location.pathname !== '/'
  ) {
    window.history.replaceState({}, document.title, '/');
  }
};
