export enum Language {
  PascaLigo = 'pascaligo',
  CameLigo = 'cameligo'
}

export enum Command {
  Compile = 'compile',
  Simulate = 'simulate',
  Originate = 'originate'
}

export enum MichelsonFormat {
  Michelson = 'michelson',
  Micheline = 'micheline'
}

const defaultExample = {
  code: `// variant defining pseudo multi-entrypoint actions
type action is
| Increment of int
| Decrement of int

function add (const a : int ; const b : int) : int is
 block { skip } with a + b

function subtract (const a : int ; const b : int) : int is
 block { skip } with a - b

// real entrypoint that re-routes the flow based
// on the action provided
function main (const p : action ; const s : int) :
  (list(operation) * int) is
 block { skip } with ((nil : list(operation)),
  case p of
  | Increment(n) -> add(s, n)
  | Decrement(n) -> subtract(s, n)
 end)`,
  language: Language.PascaLigo,
  parameters: `Increment (5)`,
  storage: '1'
};

const cameLigoExample = {
  code: `type storage = int

  (* variant defining pseudo multi-entrypoint actions *)
  type action =
    | Increment of int
    | Decrement of int
  
  let add (a: int) (b: int): int = a + b
  
  let subtract (a: int) (b: int): int = a - b
  
  (* real entrypoint that re-routes the flow based on 
     the action provided *)
  let%entry main(p : action) storage =
    let storage =
      match p with
      | Increment n -> add storage n
      | Decrement n -> subtract storage n
    in (([] : operation list), storage)`,
  language: Language.CameLigo,
  parameters: `Increment 5`,
  storage: '5'
};

export interface AppState {
  contract: string;
  command: Command;
  language: Language;
  code: string;
  output: string;
  entrypoint: string;
  parameters: string;
  storage: string;
  selectedExample: string;
  shareLink?: string;
  examples: {
    [id: string]: {
      name: string;
      code: string;
      parameters: string;
      storage: string;
      language: Language;
    };
  };
}

export class ChangeContractAction {
  public type: 'CHANGE_CONTRACT' = 'CHANGE_CONTRACT';
  constructor(public payload: AppState['contract']) {}
}

export class SelectExampleAction {
  public type: 'SELECT_EXAMPLE' = 'SELECT_EXAMPLE';
  constructor(public idx: string) {}
}

export class ChangeCommandAction {
  public type: 'CHANGE_COMMAND' = 'CHANGE_COMMAND';
  constructor(public payload: AppState['command']) {}
}

export class ChangeLanguageAction {
  public type: 'CHANGE_LANGUAGE' = 'CHANGE_LANGUAGE';
  constructor(public payload: AppState['language']) {}
}

export class ChangeShareLink {
  public type: 'CHANGE_SHARE_LINK' = 'CHANGE_SHARE_LINK';
  constructor(public payload: AppState['shareLink']) {}
}

export class ChangeOutputAction {
  public type: 'CHANGE_OUTPUT' = 'CHANGE_OUTPUT';
  constructor(public payload: AppState['output']) {}
}

export class ChangeEntrypointAction {
  public type: 'CHANGE_ENTRYPOINT' = 'CHANGE_ENTRYPOINT';
  constructor(public payload: AppState['entrypoint']) {}
}

export class ChangeParametersAction {
  public type: 'CHANGE_PARAMETERS' = 'CHANGE_PARAMETERS';
  constructor(public payload: AppState['parameters']) {}
}

export class ChangeStorageAction {
  public type: 'CHANGE_STORAGE' = 'CHANGE_STORAGE';
  constructor(public payload: AppState['storage']) {}
}

export class ChangeCodeAction {
  public type: 'CHANGE_CODE' = 'CHANGE_CODE';
  constructor(public payload: AppState['code']) {}
}

type Action =
  | ChangeCommandAction
  | ChangeLanguageAction
  | ChangeOutputAction
  | ChangeCodeAction
  | ChangeEntrypointAction
  | ChangeParametersAction
  | ChangeStorageAction
  | ChangeShareLink
  | SelectExampleAction
  | ChangeContractAction;

declare var defaultServerState: AppState | undefined;

const defaultState: AppState = {
  contract: '',
  command: Command.Compile,
  entrypoint: 'main',
  output: '',
  selectedExample: '1',
  ...defaultExample,
  examples: {
    '1': {
      name: 'PascaLIGO Contract',
      ...defaultExample
    },
    '2': {
      name: 'CameLIGO Contract',
      ...cameLigoExample
    }
  },
  ...(typeof defaultServerState === 'undefined' ? {} : defaultServerState)
};

const resetParameterToDefault = (state: AppState): AppState => {
  return {
    ...state,
    storage: '',
    entrypoint: 'main',
    output: '',
    parameters: '',
    contract: ''
  };
};

const resetShareLink = (state: AppState): AppState => {
  return { ...state, shareLink: undefined };
};

export default (state = defaultState, action: Action): AppState => {
  switch (action.type) {
    case 'CHANGE_COMMAND':
      return {
        ...state,
        command: action.payload
      };
    case 'CHANGE_LANGUAGE':
      return {
        ...resetShareLink(state),
        language: action.payload
      };
    case 'CHANGE_CODE':
      return {
        ...resetShareLink(state),
        code: action.payload
      };
    case 'CHANGE_OUTPUT':
      return {
        ...state,
        output: action.payload,
        contract: ''
      };
    case 'CHANGE_CONTRACT':
      return {
        ...state,
        contract: action.payload,
        output: ''
      };
    case 'CHANGE_ENTRYPOINT':
      return {
        ...resetShareLink(state),
        entrypoint: action.payload
      };
    case 'CHANGE_PARAMETERS':
      return {
        ...resetShareLink(state),
        parameters: action.payload
      };
    case 'CHANGE_STORAGE':
      return {
        ...resetShareLink(state),
        storage: action.payload
      };
    case 'CHANGE_SHARE_LINK':
      return {
        ...state,
        shareLink: action.payload
      };
    case 'SELECT_EXAMPLE':
      return {
        ...resetParameterToDefault(state),
        selectedExample: action.idx,
        ...state.examples[action.idx]
      };
  }
  return state;
};
