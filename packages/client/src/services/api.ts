import { Tezos } from '@taquito/taquito';
import { TezBridgeSigner } from '@taquito/tezbridge-signer';
import axios from 'axios';

import { AppState, Language } from '../redux/reducers';

Tezos.setProvider({
  rpc: 'https://api.tez.ie/rpc/babylonnet',
  signer: new TezBridgeSigner()
});

export async function compile(
  syntax: Language,
  code: string,
  entrypoint: string,
  format?: string
) {
  const response = await axios.post('/api/compile', {
    syntax,
    code,
    entrypoint,
    format
  });
  return response.data;
}

export async function simulate(
  syntax: Language,
  code: string,
  entrypoint: string,
  parameter: string,
  storage: string
) {
  const response = await axios.post('/api/simulate', {
    syntax,
    code,
    entrypoint,
    parameter,
    storage
  });
  return response.data;
}

export async function share({
  language,
  code,
  entrypoint,
  storage,
  parameters
}: Partial<AppState>) {
  const response = await axios.post('/api/share', {
    language,
    code,
    entrypoint,
    storage,
    parameters
  });
  return response.data;
}

export async function originate(code: string, storage: string) {
  return Tezos.contract.originate({ code: JSON.parse(code), init: storage });
}
