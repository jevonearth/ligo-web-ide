const commonUtils = require('./common-utils');
const clearText = commonUtils.clearText;
const waitForResponse = commonUtils.waitForResponse;
const CAMELIGO_CODE = commonUtils.CAMELIGO_CODE;

describe('Compile contract', () => {
  beforeEach(async () => {
    await page.goto('http://127.0.0.1:80');
  });

  it('should compile PascaLIGO', async done => {
    const expectedCode = `{ parameter int ; storage int ; code { DUP ; CAR ; DIP { DUP } ; SWAP ; CDR ; DUP ; DIP { DIP { DUP } ; SWAP } ; ADD ; NIL operation ; PAIR ; SWAP ; DROP ; SWAP ; DROP ; SWAP ; DROP } } `;

    await (await page.$('#entrypoint')).type('main');

    await page.click('button');
    await waitForResponse(page, 'http://127.0.0.1/api/compile');
    const code = await page.evaluate(() => {
      let element = document.getElementById('output');
      return element && element.textContent;
    });
    expect(code.replace(/\s|\n|\t/g, '')).toEqual(
      expectedCode.replace(/\s|\n|\t/g, '')
    );
    done();
  });

  it('should compile CameLIGO', async done => {
    const expectedCode = `{parameter(or(int%decrement)(int%increment));storageint;code{DUP;LAMBDA(pairintint)int{DUP;CAR;DIP{DUP};SWAP;CDR;DIP{DUP};SWAP;DIP{DUP};ADD;SWAP;DROP;SWAP;DROP;SWAP;DROP};SWAP;LAMBDA(pairintint)int{DUP;CAR;DIP{DUP};SWAP;CDR;DIP{DUP};SWAP;DIP{DUP};SUB;SWAP;DROP;SWAP;DROP;SWAP;DROP};SWAP;CAR;DIP{DIP{DIP{DUP};SWAP};SWAP};SWAP;CDR;DIP{DUP};SWAP;IF_LEFT{DUP;DIP{DIP{DUP};SWAP};SWAP;DIP{DUP};PAIR;DIP{DIP{DIP{DIP{DIP{DUP};SWAP};SWAP};SWAP};SWAP};EXEC;SWAP;DROP;SWAP;DROP}{DUP;DIP{DIP{DUP};SWAP};SWAP;DIP{DUP};PAIR;DIP{DIP{DIP{DIP{DIP{DIP{DUP};SWAP};SWAP};SWAP};SWAP};SWAP};EXEC;SWAP;DROP;SWAP;DROP};DUP;NILoperation;PAIR;DIP{DROP;DROP;DROP;DROP;DROP;DROP}}}`;

    await page.click('#editor');
    await clearText(page.keyboard);
    await page.keyboard.type(CAMELIGO_CODE);

    await page.select('#syntax', 'cameligo');
    await (await page.$('#entrypoint')).type('main');

    await page.click('button');
    await waitForResponse(page, 'http://127.0.0.1/api/compile');
    const code = await page.evaluate(() => {
      let element = document.getElementById('output');
      return element && element.textContent;
    });
    expect(code.replace(/\s|\n|\t/g, '')).toEqual(
      expectedCode.replace(/\s|\n|\t/g, '')
    );
    done();
  });

  it('should return an error when entrypoint is blank', async done => {
    const expectedOutput = '"entrypoint" is not allowed to be empty';

    await (await page.$('#entrypoint')).press('Backspace');
    await page.click('#compile');
    await waitForResponse(page, 'http://127.0.0.1/api/compile');
    const output = await page.evaluate(() => {
      let element = document.getElementById('output');
      return element && element.textContent;
    });

    expect(output).toEqual(expectedOutput);
    done();
  });

  it('should return an error when code has compilation error', async done => {
    await page.click('#editor');
    await page.keyboard.type('asdf');
    await (await page.$('#entrypoint')).type('main');
    await page.click('#compile');
    await waitForResponse(page, 'http://127.0.0.1/api/compile');
    const output = await page.evaluate(() => {
      let element = document.getElementById('output');
      return element && element.textContent;
    });

    expect(output).toMatch(
      /Parse error at \"\" from \(.*\) to \(.*\). In file .*/
    );
    done();
  });
});
