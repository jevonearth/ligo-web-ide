const commonUtils = require('./common-utils');
const clearText = commonUtils.clearText;
const waitForResponse = commonUtils.waitForResponse;
const CAMELIGO_CODE = commonUtils.CAMELIGO_CODE;

describe('Simulate contract', () => {
  beforeEach(async () => {
    await page.goto('http://127.0.0.1:80');
  });

  it('should run PascaLIGO and return correct value', async done => {
    const expectedOutput = 'tuple[list[]11]';

    await (await page.$('#entrypoint')).type('main');
    await (await page.$('#parameter')).type('5');
    await (await page.$('#storage')).type('6');
    await page.click('#simulate');
    await waitForResponse(page, 'http://127.0.0.1/api/simulate');
    const output = await page.evaluate(() => {
      let element = document.getElementById('output');
      return element && element.textContent;
    });

    expect(output.replace(/\s|\n|\t/g, '')).toEqual(
      expectedOutput.replace(/\s|\n|\t/g, '')
    );
    done();
  });

  it('should run CameLIGO and return correct value', async done => {
    const expectedOutput = 'tuple[list[]-1]';

    await page.click('#editor');
    await clearText(page.keyboard);
    await page.keyboard.type(CAMELIGO_CODE);

    await page.select('#syntax', 'cameligo');
    await (await page.$('#entrypoint')).type('main');
    await (await page.$('#parameter')).type('"Decrement 7"');
    await (await page.$('#storage')).type('6');
    await page.click('#simulate');
    await waitForResponse(page, 'http://127.0.0.1/api/simulate');
    const output = await page.evaluate(() => {
      let element = document.getElementById('output');
      return element && element.textContent;
    });

    expect(output.replace(/\s|\n|\t/g, '')).toEqual(
      expectedOutput.replace(/\s|\n|\t/g, '')
    );
    done();
  });

  it('should return an error when entrypoint is blank', async done => {
    const expectedOutput = '"entrypoint" is not allowed to be empty';

    await (await page.$('#entrypoint')).press('Backspace');
    await (await page.$('#parameter')).type('5');
    await (await page.$('#storage')).type('6');
    await page.click('#simulate');
    await waitForResponse(page, 'http://127.0.0.1/api/simulate');
    const output = await page.evaluate(() => {
      let element = document.getElementById('output');
      return element && element.textContent;
    });

    expect(output).toEqual(expectedOutput);
    done();
  });

  it('should return an error when parameter is blank', async done => {
    const expectedOutput = '"parameter" is not allowed to be empty';

    await (await page.$('#entrypoint')).type('main');
    await (await page.$('#parameter')).press('Backspace');
    await (await page.$('#storage')).type('6');
    await page.click('#simulate');
    await waitForResponse(page, 'http://127.0.0.1/api/simulate');
    const output = await page.evaluate(() => {
      let element = document.getElementById('output');
      return element && element.textContent;
    });

    expect(output).toEqual(expectedOutput);
    done();
  });

  it('should return an error when storage is blank', async done => {
    const expectedOutput = '"storage" is not allowed to be empty';

    await (await page.$('#entrypoint')).type('main');
    await (await page.$('#parameter')).type('5');
    await (await page.$('#storage')).press('Backspace');
    await page.click('#simulate');
    await waitForResponse(page, 'http://127.0.0.1/api/simulate');
    const output = await page.evaluate(() => {
      let element = document.getElementById('output');
      return element && element.textContent;
    });

    expect(output).toEqual(expectedOutput);
    done();
  });

  it('should return an error when code has compilation error', async done => {
    await page.click('#editor');
    await page.keyboard.type('asdf');
    await (await page.$('#entrypoint')).type('main');
    await (await page.$('#parameter')).type('5');
    await (await page.$('#storage')).type('6');
    await page.click('#simulate');
    await waitForResponse(page, 'http://127.0.0.1/api/simulate');
    const output = await page.evaluate(() => {
      let element = document.getElementById('output');
      return element && element.textContent;
    });

    expect(output).toMatch(
      /Parse error at \"\" from \(.*\) to \(.*\). In file .*/
    );
    done();
  });
});
