import joi from '@hapi/joi';
import { Request, Response } from 'express';
import { createHash } from 'crypto';
import { storage } from '../storage';
import { logger } from '../logger';

interface ShareBody {
  language: string;
  code: string;
  entrypoint: string;
  parameters: string;
  storage: string;
}

const validateRequest = (body: any): { value: ShareBody; error: any } => {
  return joi
    .object({
      language: joi.string().required(),
      code: joi.string().required(),
      entrypoint: joi.string(),
      parameters: joi.string(),
      storage: joi.string()
    })
    .validate(body);
};

function escapeUrl(str: string) {
  return str
    .replace(/\+/g, '-')
    .replace(/\//g, '_')
    .replace(/=/g, '');
}

export async function shareHandler(req: Request, res: Response) {
  const { error, value } = validateRequest(req.body);

  if (error) {
    res.status(400).json({ error: error.message });
  } else {
    try {
      const fileContent = JSON.stringify(value);
      const hash = createHash('md5');
      hash.update(fileContent);
      const digest = escapeUrl(hash.digest('base64'));
      const filename = `${digest}.txt`;

      storage.write(filename, fileContent);

      res.send({ hash: digest });
    } catch (ex) {
      logger.error(ex);
      res.sendStatus(500);
    }
  }
}
