import express from 'express';
import fs from 'fs';
import { dirname, join } from 'path';

import { compileHandler } from './handlers/compile';
import { shareHandler } from './handlers/share';
import { simulateHandler } from './handlers/simulate';
import { loggerMiddleware } from './logger';
import { storage } from './storage';
import { FileNotFoundError } from './storage/interface';

var bodyParser = require('body-parser');
var escape = require('escape-html');

const app = express();
const port = 8080;

const appRootDirectory =
  process.env['STATIC_ASSETS'] ||
  dirname(require.resolve('../../client/package.json'));
const appBundleDirectory = join(appRootDirectory, 'build');

app.use(bodyParser.json());
app.use(loggerMiddleware);

const file = fs.readFileSync(join(appBundleDirectory, 'index.html'));

const template = (defaultState: string = '{}') => {
  return file.toString().replace(
    `<div id="root"></div>`,
    // Injecting a script that contains a default state (Might want to refactor this if we do ssr)
    // Adding an div containing the initial state this is vulnerable to xss
    // To avoid vulnerability we escape it and then parse the content into a global variable
    `
     <input type="hidden" id="initialState" value="${escape(defaultState)}" />
     <div id="root"></div>
     <script>var defaultServerState = JSON.parse(document.getElementById("initialState").value); document.getElementById("initialState").remove()</script>`
  );
};

app.use('^/$', (req, res) => {
  res.send(template());
});

app.get(`/p/:hash([0-9a-zA-Z\-\_]+)`, async (req, res) => {
  try {
    const content = await storage.read(`${req.params['hash']}.txt`);

    const state = { ...JSON.parse(content), shareLink: req.params['hash'] };
    res.send(template(JSON.stringify(state)));
  } catch (ex) {
    if (ex instanceof FileNotFoundError) {
      res.sendStatus(404);
    } else {
      res.sendStatus(500);
    }
  }
});

app.use(express.static(appBundleDirectory));
app.post('/api/compile', compileHandler);
app.post('/api/simulate', simulateHandler);
app.post('/api/share', shareHandler);

app.listen(port, () => {
  console.log(`Listening on: ${port}`);
});
